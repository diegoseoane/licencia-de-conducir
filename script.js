const nombre = document.querySelector('#nombre');
const apellido = document.querySelector('#apellido');
const edad = document.querySelector('#edad');
const licencia = document.querySelector('#licencia');
const fecha = document.querySelector('#fecha');
const button = document.querySelector('#button');
const error = document.querySelector('#error');
const ok = document.querySelector('#ok');

button.addEventListener('click', function(e) {

	const selectedOption = licencia.options[licencia.selectedIndex].value;
	
	let date = new Date();
	let day = date.getDate();
	let month = date.getMonth() + 1;
	let year = date.getFullYear();

	const fullDate = month < 9 ? year + "0" + month + day : year + month + day;
	
	if (nombre.value === ""){
		error.innerHTML = "El campo 'Nombre' no puede estar vacio";
		ok.innerHTML = "";
	} else if (apellido.value === "") {
		error.innerHTML = "El campo 'Apellido' no puede estar vacio";
		ok.innerHTML = "";
	} else if (edad.value === "") {
		error.innerHTML = "El campo 'Edad' no puede estar vacio";
		ok.innerHTML = "";
	} else if (selectedOption != 1 && selectedOption != 2) {
		error.innerHTML = "El campo '¿Tiene licencia de conducir?' no puede estar vacio";
		ok.innerHTML = "";
	} else if (fecha.value === "") {
		error.innerHTML = "El campo 'Fecha de Expiración' no puede estar vacio";
		ok.innerHTML = "";
	} else if (parseInt(edad.value) < 18) {
		error.innerHTML = "Usted es menor de edad";	
		ok.innerHTML = "";
	} else if (selectedOption === "2") {
		error.innerHTML = "Consígase una licencia de conducir";
		ok.innerHTML = "";
	} else if (fecha.value.length !== 8) {
		error.innerHTML = "Formato de fecha inválido";
		ok.innerHTML = "";
	} else if (parseInt(fecha.value) < parseInt(fullDate)){
		error.innerHTML = "Su licencia está expirada";
		ok.innerHTML = "";
	} else { 
		error.innerHTML = ""; 
		ok.innerHTML = "Felicitaciones. Usted puede conducir";
	}
		
});